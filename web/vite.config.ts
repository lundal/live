import { defineConfig } from "vite";
import solidPlugin from "vite-plugin-solid";

export default defineConfig({
  build: {
    assetsInlineLimit: 256,
    minify: true,
  },
  plugins: [solidPlugin()],
  server: {
    port: 3103,
    proxy: { "/api": "http://127.0.0.1:3203" },
  },
});
