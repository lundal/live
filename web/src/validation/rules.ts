import { Rule } from "./validator.ts";

const integerPattern = /^-?[0-9]+$/;
const floatPattern = /^-?[0-9]+(\.[0-9]+)?$/;
const alphanumPattern = /^[A-Za-z0-9]+$/;
const isoDatePattern = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;

export function required(): Rule {
  return {
    isValid: (value) => !!value,
    message: (name) => `${name} is required`,
  };
}

export function minChars(n: number): Rule {
  return {
    isValid: (value) => value.length >= n,
    message: (name) => `${name} must be at least ${n} characters`,
  };
}

export function maxChars(n: number): Rule {
  return {
    isValid: (value) => value.length <= n,
    message: (name) => `${name} must be at most ${n} characters`,
  };
}

export function chars(n: number): Rule {
  return {
    isValid: (value) => value.length == n,
    message: (name) => `${name} must be exactly ${n} characters`,
  };
}

export function charsBetween(min: number, max: number): Rule {
  return {
    isValid: (value) => value.length >= min && value.length <= max,
    message: (name) => `${name} must be between ${min} and ${max} characters`,
  };
}

export function integer(min: number, max: number): Rule {
  return {
    isValid: (value) =>
      !!value.match(integerPattern) &&
      parseInt(value) >= min &&
      parseInt(value) <= max,
    message: (name) =>
      `${name} must be a whole number between ${min} and ${max}`,
  };
}

export function float(min: number, max: number): Rule {
  return {
    isValid: (value) =>
      !!value.match(floatPattern) &&
      parseFloat(value) >= min &&
      parseFloat(value) <= max,
    message: (name) => `${name} must be a number between ${min} and ${max}`,
  };
}

export function alphanum(): Rule {
  return {
    isValid: (value) => !!value.match(alphanumPattern),
    message: (name) => `${name} must contain only letters and digits`,
  };
}

export function isoDate(): Rule {
  return {
    isValid: (value) => {
      const match = value.match(isoDatePattern);
      if (!match) {
        return false;
      }
      const year = parseInt(match[1]);
      const month = parseInt(match[2]);
      const day = parseInt(match[3]);
      const date = new Date(year, month - 1, day);
      return (
        date.getFullYear() == year &&
        date.getMonth() == month - 1 &&
        date.getDate() == day
      );
    },
    message: (name) => `${name} must be a valid date (YYYY-MM-DD)`,
  };
}

export function regex(pattern: RegExp, text: string): Rule {
  return {
    isValid: (value) => !!value.match(pattern),
    message: (name) => `${name} ${text}`,
  };
}
