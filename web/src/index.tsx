import { createResource, JSX, Match, Show, Switch } from "solid-js";
import { render } from "solid-js/web";
import { Navigate, Route, Router } from "@solidjs/router";
import { getConfig } from "./api/config";
import { LoadingPage } from "./pages/LoadingPage";
import { ListBoardsPage } from "./pages/ListBoardsPage.tsx";
import { CreateBoardPage } from "./pages/CreateBoardPage";
import { RedirectToSignInPage } from "./pages/RedirectToSignInPage.tsx";
import { BoardPage } from "./pages/BoardPage.tsx";

function App(): JSX.Element {
  const [config] = createResource(getConfig);

  return (
    <Router>
      <Show when={config()} fallback={<LoadingPage />}>
        {(config) => (
          <Switch>
            <Match when={config().account}>
              {(_account) => (
                <>
                  <Route path="/boards" component={() => <ListBoardsPage />} />
                  <Route
                    path="/boards/create"
                    component={() => <CreateBoardPage />}
                  />
                  <Route
                    path="/boards/:id"
                    component={(props) => <BoardPage id={props.params.id} />}
                  />
                  <Route
                    path="*"
                    component={() => <Navigate href="/boards" />}
                  />
                </>
              )}
            </Match>
            <Match when={!config().account}>
              <Route
                path="/boards/:id"
                component={(props) => <BoardPage id={props.params.id} />}
              />
              <Route
                path="*"
                component={() => (
                  <RedirectToSignInPage accountUrl={config().accountUrl} />
                )}
              />
            </Match>
          </Switch>
        )}
      </Show>
    </Router>
  );
}

render(() => <App />, document.getElementById("app")!);
