export type Position = {
  col: number;
  row: number;
};

export type Area = {
  col: number;
  row: number;
  width: number;
  height: number;
};
