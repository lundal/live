import { JSX } from "solid-js";
import { Page, Header, Spinner } from "@lundal/zed-solid";
import { AppIcon } from "../components/AppIcon.tsx";

export function LoadingPage(): JSX.Element {
  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Live" />
      <main style={{ padding: "24px" }}>
        <Spinner style={{ margin: "0 auto" }} size="large" />
      </main>
    </Page>
  );
}
