import { createResource, createSignal, JSX, Show } from "solid-js";
import { Header, Icon, IconButton, Page, Spinner } from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import { getBoard } from "../api/boards";
import { faCog, faExpand } from "@fortawesome/free-solid-svg-icons";
import { BoardGrid } from "../components/BoardGrid";
import { SettingsPanel } from "../components/SettingsPanel";
import { SelectionMode } from "../components/BoardGrid";
import { Area } from "../common/Position";
import { AppIcon } from "../components/AppIcon.tsx";
import { ErrorMessage } from "../components/ErrorMessage.tsx";

type State = View | EditBoard | EditWidget | MoveWidget;

type View = {
  type: "view";
};

type EditBoard = {
  type: "edit-board";
};

type EditWidget = {
  type: "edit-widget";
  widgetId: string;
};

type MoveWidget = {
  type: "move-widget";
  widgetId: string;
  callback: (area: Area) => void;
};

type Props = {
  id: string;
};

export function BoardPage(props: Props): JSX.Element {
  const [board, boardActions] = createResource(props.id, (id) => getBoard(id));

  const [state, setState] = createSignal<State>({ type: "view" });

  function getSelectionMode(): SelectionMode {
    const _state = state();
    switch (_state.type) {
      case "view":
        return "none";
      case "edit-board":
      case "edit-widget":
        return "widget";
      case "move-widget":
        return "area";
    }
  }

  function getSelectedWidget(): string | undefined {
    const _state = state();
    switch (_state.type) {
      case "edit-widget":
      case "move-widget":
        return _state.widgetId;
      default:
        return undefined;
    }
  }

  function onSelectWidget(id: string) {
    const _state = state();
    switch (_state.type) {
      case "edit-board":
      case "edit-widget":
        return setState({ type: "edit-widget", widgetId: id });
      default:
        return;
    }
  }

  function onMoveWidget(callback: (area: Area) => void) {
    const _state = state();
    switch (_state.type) {
      case "edit-widget":
        return setState({
          type: "move-widget",
          widgetId: _state.widgetId,
          callback: callback,
        });
      default:
        return;
    }
  }

  function onSelectArea(area: Area) {
    const _state = state();
    switch (_state.type) {
      case "move-widget":
        _state.callback(area);
        return setState({ type: "edit-widget", widgetId: _state.widgetId });
      default:
        return;
    }
  }

  function renderBoard(): JSX.Element {
    switch (board.state) {
      case "unresolved":
      case "pending":
        return <Spinner size="large" />;
      case "ready":
      case "refreshing":
        return (
          <>
            <BoardGrid
              board={board.latest}
              selectionMode={getSelectionMode()}
              selectedWidget={getSelectedWidget()}
              onSelectWidget={onSelectWidget}
              onSelectArea={onSelectArea}
            />
            {state().type != "view" && (
              <SettingsPanel
                board={board.latest}
                selected={getSelectedWidget()}
                refreshBoard={boardActions.refetch}
                onSelectWidget={onSelectWidget}
                onMoveWidget={onMoveWidget}
              />
            )}
          </>
        );
      case "errored":
        return <ErrorMessage error={board.error as RequestError} />;
    }
  }

  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Live">
        <Show when={!board.error && board.latest}>
          <div style={{ display: "flex" }}>
            <IconButton
              type="tertiary"
              icon={<Icon definition={faExpand} />}
              label="Enter full screen mode"
              onClick={() => {
                document
                  .querySelector(".board-grid")
                  ?.requestFullscreen({ navigationUI: "hide" })
                  .then(() => setState({ type: "view" }));
              }}
            />
            <IconButton
              type="tertiary"
              icon={<Icon definition={faCog} />}
              label="Configure board"
              onClick={() => {
                if (state().type == "view") {
                  setState({ type: "edit-board" });
                } else {
                  setState({ type: "view" });
                }
              }}
            />
          </div>
        </Show>
      </Header>
      {renderBoard()}
    </Page>
  );
}
