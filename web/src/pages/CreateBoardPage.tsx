import { JSX } from "solid-js";
import {
  Button,
  Card,
  Column,
  Field,
  Form,
  H1,
  Header,
  Page,
  TextBox,
  createForm,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import { Board } from "../api/models";
import { createBoard } from "../api/boards";
import { validate } from "../validation/validator.ts";
import { maxChars, minChars, required } from "../validation/rules.ts";
import { AppIcon } from "../components/AppIcon.tsx";
import { useNavigate } from "@solidjs/router";
import { ErrorMessage } from "../components/ErrorMessage.tsx";

type Fields = {
  name: string;
};

type Errors = {
  name?: string;
};

const nameRules = [required(), minChars(3), maxChars(60)];

export function CreateBoardPage(): JSX.Element {
  const navigate = useNavigate();
  const form = createForm<Fields, Errors, Board, RequestError>({
    initiator: () => ({
      name: "",
    }),
    validator: (fields) => ({
      name: validate("Name", fields.name, nameRules),
    }),
    submitter: (fields) =>
      createBoard({
        name: fields.name,
      }),
    onSuccess: (board) => {
      navigate(`/boards/${board.id}`);
    },
  });

  function errorMessage(): JSX.Element {
    const state = form.state();
    if (state.type == "Failure") {
      return <ErrorMessage error={state.error} />;
    }
  }

  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Live" />
      <main style={{ padding: "24px" }}>
        <Card style={{ margin: "0 auto", "max-width": "480px" }}>
          <Form onSubmit={form.submit}>
            <Column>
              <H1 lookLike="h2">Create board</H1>
              <div style={{ width: "100%" }}>
                <Field
                  id="name"
                  label="Name"
                  error={form.errors().name}
                  children={(extra) => (
                    <TextBox
                      {...extra()}
                      value={form.fields().name}
                      onChange={(value) => form.update({ name: value })}
                    />
                  )}
                />
              </div>
              <Button
                type="primary"
                label="Create board"
                onClick="submit"
                busy={form.busy()}
              />
              {errorMessage()}
            </Column>
          </Form>
        </Card>
      </main>
    </Page>
  );
}
