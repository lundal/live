import { createResource, For, JSX, Match, Switch } from "solid-js";
import {
  Card,
  Column,
  H1,
  Header,
  Link,
  Page,
  Spinner,
} from "@lundal/zed-solid";
import { listBoards } from "../api/boards";
import { AppIcon } from "../components/AppIcon.tsx";
import { ErrorMessage } from "../components/ErrorMessage.tsx";

export function ListBoardsPage(): JSX.Element {
  const [boards] = createResource(listBoards);

  return (
    <Page layout="3" theme="dark">
      <Header appIcon={<AppIcon />} appName="Live" />
      <main style={{ padding: "24px" }}>
        <Card style={{ margin: "0 auto", "max-width": "480px" }}>
          <Column>
            <H1 lookLike="h2">Boards</H1>
            <Switch fallback={<Spinner />}>
              <Match when={boards.error}>
                <ErrorMessage error={boards.error} />
              </Match>
              <Match when={boards.latest}>
                <For each={boards.latest}>
                  {(board) => (
                    <p>
                      <Link href={`/boards/${board.id}`}>{board.name}</Link>
                    </p>
                  )}
                </For>
              </Match>
            </Switch>
            <p>
              <Link href={"/boards/create"}>Create board</Link>
            </p>
          </Column>
        </Card>
      </main>
    </Page>
  );
}
