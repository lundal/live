import { JSX, onMount } from "solid-js";
import { LoadingPage } from "./LoadingPage.tsx";

type Props = {
  accountUrl: String;
};

export function RedirectToSignInPage(props: Props): JSX.Element {
  onMount(() => {
    window.location.href =
      props.accountUrl +
      "/sign-in?return=" +
      encodeURIComponent(window.location.href);
  });
  return <LoadingPage />;
}
