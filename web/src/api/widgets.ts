import { request } from "@lundal/request";
import {
  DirectionType,
  EnturData,
  Language,
  TransportMode,
  YrData,
} from "./models";

export function getEnturData(
  stopPlaceId: string,
  transportMode: TransportMode,
  directionType: DirectionType,
  language: Language,
): Promise<EnturData> {
  return request({
    method: "GET",
    url:
      "/api/v1/widgets/entur" +
      `?stopPlaceId=${stopPlaceId}` +
      `&transportMode=${transportMode}` +
      `&directionType=${directionType}` +
      `&language=${language}`,
  });
}

export function getYrData(
  latitude: number,
  longitude: number,
  altitude: number,
): Promise<YrData> {
  return request({
    method: "GET",
    url:
      "/api/v1/widgets/yr" +
      `?latitude=${latitude}` +
      `&longitude=${longitude}` +
      `&altitude=${altitude}`,
  });
}
