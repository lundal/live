export type Config = {
  account?: Account;
  accountUrl: string;
  appLinks: AppLink[];
};

export type Account = {
  id: string;
  username: string;
};

export type AppLink = {
  name: string;
  iconUrl: string;
  linkUrl: string;
};

export type UpdateBoard = {
  name: string;
};

export type Board = {
  id: string;
  name: string;
  widgets: Widget[];
};

export type Widget<T extends WidgetConfig = WidgetConfig> = {
  id: string;
  col: number;
  row: number;
  width: number;
  height: number;
  config: T;
};

export type UpdateWidget<T extends WidgetConfig = WidgetConfig> = {
  col: number;
  row: number;
  width: number;
  height: number;
  config: T;
};

export type WidgetConfig =
  | AnniversariesConfig
  | ClockConfig
  | EnturConfig
  | YrConfig;

export type AnniversariesConfig = {
  type: "anniversaries";
  anniversaries: Anniversary[];
  language: Language;
};

export type Anniversary = {
  name: string;
  date: string;
};

export type ClockConfig = {
  type: "clock";
};

export type EnturConfig = {
  type: "entur";
  title: string;
  stopPlaceId: string;
  transportMode: TransportMode;
  directionType: DirectionType;
  language: Language;
};

export type TransportMode =
  | "AIR"
  | "BUS"
  | "COACH"
  | "METRO"
  | "RAIL"
  | "TRAM"
  | "WATER";

export type DirectionType = "INBOUND" | "OUTBOUND";

export type Language = "ENGLISH" | "NORWEGIAN";

export type EnturData = {
  departures: Departure[];
};

export type Departure = {
  timestamp: number;
  direction: string;
  destination: string;
  line: Line;
  situations: string[];
};

export type Line = {
  code: string;
  name: string;
  color: string;
  textColor: string;
};

export type YrConfig = {
  type: "yr";
  latitude: number;
  longitude: number;
  altitude: number;
};

export type YrData = {
  forecasts: WeatherForecast[];
};

export type WeatherForecast = {
  timestamp: number;
  symbol: string;
  temperature: number;
  precipitation: number;
  wind: number;
};
