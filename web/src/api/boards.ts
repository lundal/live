import { request } from "@lundal/request";
import { Board, UpdateBoard, UpdateWidget, Widget } from "./models";

export function createBoard(board: UpdateBoard): Promise<Board> {
  return request({
    method: "POST",
    url: `/api/v1/boards`,
    body: board,
  });
}

export function updateBoard(
  boardId: string,
  board: UpdateBoard,
): Promise<Board> {
  return request({
    method: "PUT",
    url: `/api/v1/boards/${boardId}`,
    body: board,
  });
}

export function deleteBoard(boardId: string): Promise<void> {
  return request({
    method: "DELETE",
    url: `/api/v1/boards/${boardId}`,
  });
}

export function createWidget(
  boardId: string,
  widget: UpdateWidget,
): Promise<Widget> {
  return request({
    method: "POST",
    url: `/api/v1/boards/${boardId}/widgets`,
    body: widget,
  });
}

export function updateWidget(
  boardId: string,
  widgetId: string,
  widget: UpdateWidget,
): Promise<Widget> {
  return request({
    method: "PUT",
    url: `/api/v1/boards/${boardId}/widgets/${widgetId}`,
    body: widget,
  });
}

export function deleteWidget(boardId: string, widgetId: string): Promise<void> {
  return request({
    method: "DELETE",
    url: `/api/v1/boards/${boardId}/widgets/${widgetId}`,
  });
}

export function listBoards(): Promise<Board[]> {
  return request({
    method: "GET",
    url: `/api/v1/boards`,
  });
}

export function getBoard(boardId: string): Promise<Board> {
  return request({
    method: "GET",
    url: `/api/v1/boards/${boardId}`,
  });
}
