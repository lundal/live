import { request } from "@lundal/request";
import { Config } from "./models";

export function getConfig(): Promise<Config> {
  return request({
    method: "GET",
    url: "/api/v1/config",
  });
}
