import { JSX, Show } from "solid-js";
import { Button, Column, H2, Row } from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import {
  AnniversariesConfig,
  Board,
  ClockConfig,
  EnturConfig,
  Widget,
  WidgetConfig,
  YrConfig,
} from "../api/models";
import { PositionForm } from "./PositionForm";
import { AnniversariesForm } from "./widgets/AnniversariesForm";
import { EnturForm } from "./widgets/EnturForm";
import { YrForm } from "./widgets/YrForm";
import { createWidget, deleteWidget, updateWidget } from "../api/boards";
import { Area } from "../common/Position";
import "./SettingsPanel.css";

type Props = {
  board: Board;
  selected?: string;
  refreshBoard: () => void;
  onSelectWidget: (id: string) => void;
  onMoveWidget: (callback: (area: Area) => void) => void;
};

export function SettingsPanel(props: Props): JSX.Element {
  function getSelectedWidget() {
    return props.board.widgets.find((it) => it.id == props.selected);
  }

  function createButton(label: string, config: WidgetConfig): JSX.Element {
    return (
      <Button
        type="secondary"
        label={label}
        onClick={() => {
          createWidget(props.board.id, {
            col: 1,
            row: 1,
            width: 36,
            height: 12,
            config: config,
          })
            .then((widget: Widget) => {
              props.refreshBoard();
              props.onSelectWidget(widget.id);
            })
            .catch((error: RequestError) => alert(error.message));
        }}
      />
    );
  }

  function moveButton(widget: Widget): JSX.Element {
    return (
      <Button
        type="secondary"
        label="Move"
        onClick={() =>
          props.onMoveWidget((area: Area) => {
            updateWidget(props.board.id, widget.id, {
              col: area.col,
              row: area.row,
              width: area.width,
              height: area.height,
              config: widget.config,
            })
              .then(() => props.refreshBoard())
              .catch((error: RequestError) => alert(error.message));
          })
        }
      />
    );
  }

  function deleteButton(widget: Widget): JSX.Element {
    return (
      <Button
        type="secondary"
        label="Delete"
        onClick={() => {
          deleteWidget(props.board.id, widget.id)
            .then(() => props.refreshBoard())
            .catch((error: RequestError) => alert(error.message));
        }}
      />
    );
  }

  function positionForm(widget: Widget): JSX.Element {
    return (
      <PositionForm
        boardId={props.board.id}
        widget={widget}
        onUpdate={props.refreshBoard}
      />
    );
  }

  function configForm(widget: Widget): JSX.Element | null {
    switch (widget.config.type) {
      case "anniversaries": {
        return (
          <AnniversariesForm
            boardId={props.board.id}
            widget={widget as Widget<AnniversariesConfig>}
            onUpdate={props.refreshBoard}
          />
        );
      }
      case "entur": {
        return (
          <EnturForm
            boardId={props.board.id}
            widget={widget as Widget<EnturConfig>}
            onUpdate={props.refreshBoard}
          />
        );
      }
      case "yr":
        return (
          <YrForm
            boardId={props.board.id}
            widget={widget as Widget<YrConfig>}
            onUpdate={props.refreshBoard}
          />
        );
      default:
        return null;
    }
  }

  return (
    <aside class="settings-panel">
      <Column>
        <H2>Create widget</H2>
        <Row>
          {createButton("Anniversaries", defaultAnniversariesConfig)}
          {createButton("Clock", defaultClockConfig)}
          {createButton("Entur", defaultEnturConfig)}
          {createButton("Yr", defaultYrConfig)}
        </Row>
        <Show when={getSelectedWidget()}>
          {(widget) => (
            <>
              <H2>Modify widget</H2>
              <Row>
                {moveButton(widget())}
                {deleteButton(widget())}
              </Row>
              {positionForm(widget())}
              {configForm(widget())}
            </>
          )}
        </Show>
      </Column>
    </aside>
  );
}

const defaultAnniversariesConfig: AnniversariesConfig = {
  type: "anniversaries",
  anniversaries: [],
  language: "ENGLISH",
};

const defaultClockConfig: ClockConfig = {
  type: "clock",
};

const defaultEnturConfig: EnturConfig = {
  type: "entur",
  title: "Oslo S",
  stopPlaceId: "NSR:StopPlace:337",
  transportMode: "RAIL",
  directionType: "INBOUND",
  language: "ENGLISH",
};

const defaultYrConfig: YrConfig = {
  type: "yr",
  latitude: 51.53,
  longitude: -0.24,
  altitude: 20,
};
