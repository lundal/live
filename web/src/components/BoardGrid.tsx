import { createSignal, For, JSX, Show } from "solid-js";
import { createSize } from "@lundal/zed-solid";
import { Board, Widget } from "../api/models";
import { Area, Position } from "../common/Position";
import { AnniversariesWidget } from "./widgets/AnniversariesWidget";
import { ClockWidget } from "./widgets/ClockWidget";
import { EnturWidget } from "./widgets/EnturWidget";
import { YrWidget } from "./widgets/YrWidget";
import "./BoardGrid.css";

export type SelectionMode = "none" | "widget" | "area";

type Props = {
  board: Board;
  selectionMode: SelectionMode;
  selectedWidget?: string;
  onSelectWidget: (id: string) => void;
  onSelectArea: (area: Area) => void;
};

export function BoardGrid(props: Props): JSX.Element {
  let element!: HTMLElement;

  const size = createSize(() => element);
  const [currentPosition, setCurrentPosition] = createSignal<Position>();
  const [fromPosition, setFromPosition] = createSignal<Position>();

  function area(): Area | undefined {
    const _currentPosition = currentPosition();
    const _fromPosition = fromPosition();
    if (_currentPosition && _fromPosition) {
      return toArea(_currentPosition, _fromPosition);
    }
    if (_currentPosition) {
      return toArea(_currentPosition, _currentPosition);
    }
    return undefined;
  }

  function renderWidget(widget: Widget): JSX.Element {
    switch (widget.config.type) {
      case "anniversaries":
        return <AnniversariesWidget config={widget.config} />;
      case "clock":
        return <ClockWidget config={widget.config} />;
      case "entur":
        return <EnturWidget config={widget.config} />;
      case "yr":
        return <YrWidget config={widget.config} />;
    }
    return null;
  }

  return (
    <main
      ref={element}
      class="board-grid"
      onMouseMove={(e) => {
        if (props.selectionMode == "area") {
          const x = e.pageX - e.currentTarget.offsetLeft;
          const y = e.pageY - e.currentTarget.offsetTop;
          setCurrentPosition(toPosition(x, y, size().width, size().height));
        }
      }}
      onMouseDown={(e) => {
        if (props.selectionMode == "area") {
          e.preventDefault();
          setFromPosition(currentPosition());
        }
      }}
      onMouseUp={() => {
        if (props.selectionMode == "area") {
          const _area = area();
          _area && props.onSelectArea(_area);
          setFromPosition(undefined);
        }
      }}
    >
      <For each={props.board.widgets}>
        {(widget, index) => (
          <div
            style={{
              "grid-column": `${widget.col} / span ${widget.width}`,
              "grid-row": `${widget.row} / span ${widget.height}`,
              "z-index": 100 + index(),
            }}
            classList={{ selected: props.selectedWidget == widget.id }}
            onClick={() => {
              if (props.selectionMode == "widget") {
                props.onSelectWidget(widget.id);
              }
            }}
          >
            {renderWidget(widget)}
          </div>
        )}
      </For>
      <Show when={props.selectionMode == "area" && area()}>
        {(area) => (
          <div
            style={{
              "grid-column": `${area().col} / span ${area().width}`,
              "grid-row": `${area().row} / span ${area().height}`,
              "z-index": 200,
            }}
            class="preview"
          />
        )}
      </Show>
    </main>
  );
}

function toArea(a: Position, b: Position): Area {
  return {
    col: Math.min(a.col, b.col),
    row: Math.min(a.row, b.row),
    width: Math.abs(a.col - b.col) + 1,
    height: Math.abs(a.row - b.row) + 1,
  };
}

function toPosition(
  x: number,
  y: number,
  width: number,
  height: number,
): Position {
  // Should match css
  const cols = 36;
  const rows = 36;
  const gap = 12;

  const col = Math.floor(((x - gap / 2) / (width - gap)) * cols + 1);
  const row = Math.floor(((y - gap / 2) / (height - gap)) * rows + 1);

  return { col: clamp(1, col, cols), row: clamp(1, row, rows) };
}

function clamp(min: number, value: number, max: number): number {
  return Math.max(min, Math.min(value, max));
}
