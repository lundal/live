import { createEffect, JSX, on } from "solid-js";
import {
  Button,
  Column,
  Field,
  Form,
  H2,
  Row,
  TextBox,
  createForm,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import { Widget } from "../api/models";
import { updateWidget } from "../api/boards";
import { integer, required } from "../validation/rules.ts";
import { validate } from "../validation/validator.ts";
import { ErrorMessage } from "./ErrorMessage.tsx";

type Fields = {
  col: string;
  row: string;
  width: string;
  height: string;
};

type Errors = {
  col?: string;
  row?: string;
  width?: string;
  height?: string;
};

type Props = {
  boardId: string;
  widget: Widget;
  onUpdate: () => void;
};

const positionRules = [required(), integer(1, 36)];

export function PositionForm(props: Props): JSX.Element {
  const form = createForm<Fields, Errors, Widget, RequestError>({
    initiator: () => ({
      col: props.widget.col.toString(),
      row: props.widget.row.toString(),
      width: props.widget.width.toString(),
      height: props.widget.height.toString(),
    }),
    validator: (fields) => ({
      col: validate("Column", fields.col, positionRules),
      row: validate("Row", fields.row, positionRules),
      width: validate("Width", fields.width, positionRules),
      height: validate("Height", fields.height, positionRules),
    }),
    submitter: (fields) =>
      updateWidget(props.boardId, props.widget.id, {
        col: parseInt(fields.col),
        row: parseInt(fields.row),
        width: parseInt(fields.width),
        height: parseInt(fields.height),
        config: props.widget.config,
      }),
    onSuccess: () => props.onUpdate(),
  });

  createEffect(
    on(
      () => props.widget,
      () => {
        console.log("reset position form");
        form.reset();
      },
    ),
  );

  function errorMessage(): JSX.Element {
    const state = form.state();
    if (state.type == "Failure") {
      return <ErrorMessage error={state.error} />;
    }
  }

  return (
    <Form onSubmit={form.submit}>
      <Column>
        <H2>Position</H2>
        <Row>
          <Field
            id="col"
            label="Column"
            // help="Number (1 - 36)"
            error={form.errors().col}
            errorPlaceholder={false}
            children={(extra) => (
              <TextBox
                {...extra()}
                value={form.fields().col}
                onChange={(value) => form.update({ col: value })}
              />
            )}
          />
          <Field
            id="row"
            label="Row"
            // help="Number (1 - 36)"
            error={form.errors().row}
            errorPlaceholder={false}
            children={(extra) => (
              <TextBox
                {...extra()}
                value={form.fields().row}
                onChange={(value) => form.update({ row: value })}
              />
            )}
          />
        </Row>
        <Row>
          <Field
            id="width"
            label="Width"
            // help="Number (1 - 36)"
            error={form.errors().width}
            errorPlaceholder={false}
            children={(extra) => (
              <TextBox
                {...extra()}
                value={form.fields().width}
                onChange={(value) => form.update({ width: value })}
              />
            )}
          />
          <Field
            id="height"
            label="Height"
            // help="Number (1 - 36)"
            error={form.errors().height}
            errorPlaceholder={false}
            children={(extra) => (
              <TextBox
                {...extra()}
                value={form.fields().height}
                onChange={(value) => form.update({ height: value })}
              />
            )}
          />
        </Row>
        <Button
          type="primary"
          label="Save"
          onClick="submit"
          busy={form.busy()}
        />
        {errorMessage()}
      </Column>
    </Form>
  );
}
