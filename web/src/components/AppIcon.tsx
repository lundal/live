import { JSX } from "solid-js";

export function AppIcon(): JSX.Element {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="360"
      height="360"
      viewBox="0 0 360 360"
      style="--bg: currentColor; --fg: var(--z-foreground);"
    >
      <rect width="360" height="360" rx="60" fill="var(--bg)" />
      <circle cx="180" cy="180" r="120" fill="var(--fg)" />
      <circle
        cx="180"
        cy="180"
        r="90"
        fill="var(--fg)"
        stroke="var(--bg)"
        stroke-width="20"
      />
    </svg>
  );
}
