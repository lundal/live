import {
  Accessor,
  createMemo,
  createResource,
  For,
  JSX,
  Match,
  Switch,
} from "solid-js";
import { createInterval, Spinner } from "@lundal/zed-solid";
import { EnturConfig, EnturData } from "../../api/models";
import { getEnturData } from "../../api/widgets";
import "./EnturWidget.css";
import { ErrorMessage } from "../ErrorMessage.tsx";

type Props = {
  config: EnturConfig;
};

export function EnturWidget(props: Props): JSX.Element {
  const [data, dataActions] = createResource(props.config, () =>
    getEnturData(
      props.config.stopPlaceId,
      props.config.transportMode,
      props.config.directionType,
      props.config.language,
    ),
  );

  const now = createInterval(() => Date.now());

  createInterval(() => dataActions.refetch(), 60_000);

  function durationAsText(milliseconds: number): string {
    const seconds = Math.floor(milliseconds / 1000);
    const minutes = Math.floor(seconds / 60);

    if (minutes > 0) {
      return `${minutes}m`;
    }

    switch (props.config.language) {
      case "ENGLISH":
        return "now";
      case "NORWEGIAN":
        return "nå";
    }
  }

  function listDepartures(data: Accessor<EnturData>): JSX.Element {
    const departures = createMemo(() =>
      data().departures.filter((data) => data.timestamp > now()),
    );
    return (
      <For each={departures()}>
        {(departure) => (
          <div class="departure">
            <div
              class="code"
              style={{
                background: `#${departure.line.color}`,
                color: `#${departure.line.textColor}`,
              }}
            >
              {departure.line.code}
            </div>
            <div class="destination">{departure.destination}</div>
            <div class="time">
              {durationAsText(departure.timestamp - now())}
            </div>
          </div>
        )}
      </For>
    );
  }

  return (
    <div class="entur">
      <div class="title">{props.config.title}</div>
      <Switch fallback={<Spinner />}>
        <Match when={data.error}>
          <ErrorMessage error={data.error} />
        </Match>
        <Match when={data.latest}>{(latest) => listDepartures(latest)}</Match>
      </Switch>
      <div class="fade"></div>
    </div>
  );
}
