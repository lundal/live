import { createEffect, Index, JSX, on } from "solid-js";
import {
  Button,
  Column,
  Form,
  H2,
  Icon,
  IconButton,
  RadioButtons,
  Row,
  TextBox,
  createForm,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import {
  AnniversariesConfig,
  Anniversary,
  Language,
  Widget,
} from "../../api/models";
import { updateWidget } from "../../api/boards";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import {
  isoDate,
  maxChars,
  minChars,
  required,
} from "../../validation/rules.ts";
import { validate } from "../../validation/validator.ts";
import { ErrorMessage } from "../ErrorMessage.tsx";
import "./AnniversariesForm.css";

type Fields = {
  anniversaries: Anniversary[];
  language: Language;
};

type Errors = {
  anniversaries?: {
    name?: string;
    date?: string;
  }[];
};

type Props = {
  boardId: string;
  widget: Widget<AnniversariesConfig>;
  onUpdate: () => void;
};

const nameRules = [required(), minChars(2), maxChars(64)];
const dateRules = [required(), isoDate()];

export function AnniversariesForm(props: Props): JSX.Element {
  const form = createForm<Fields, Errors, Widget, RequestError>({
    initiator: () => ({
      anniversaries: props.widget.config.anniversaries,
      language: props.widget.config.language,
    }),
    validator: (fields) => ({
      anniversaries: fields.anniversaries.map((anniversary) => ({
        name: validate("Name", anniversary.name, nameRules),
        date: validate("Date", anniversary.date, dateRules),
      })),
    }),
    submitter: (fields) =>
      updateWidget(props.boardId, props.widget.id, {
        col: props.widget.col,
        row: props.widget.row,
        width: props.widget.width,
        height: props.widget.height,
        config: {
          type: "anniversaries",
          anniversaries: fields.anniversaries,
          language: fields.language,
        },
      }),
    onSuccess: () => props.onUpdate(),
  });

  createEffect(
    on(
      () => props.widget,
      () => {
        console.log("reset anniversaries form");
        form.reset();
      },
    ),
  );

  function addAnniversary(anniversary: Anniversary) {
    form.update({
      anniversaries: [...form.fields().anniversaries, anniversary],
    });
  }

  function removeAnniversary(index: number) {
    form.update({
      anniversaries: form.fields().anniversaries.filter((_, i) => i !== index),
    });
  }

  function updateAnniversary(index: number, anniversary: Partial<Anniversary>) {
    form.update({
      anniversaries: form
        .fields()
        .anniversaries.map((prevAnniversary, i) =>
          i === index
            ? { ...prevAnniversary, ...anniversary }
            : prevAnniversary,
        ),
    });
  }

  function sortAnniversaries() {
    form.update({
      anniversaries: form.fields().anniversaries.sort((a, b) => {
        const aDate = new Date(a.date);
        const bDate = new Date(b.date);
        const aScore = aDate.getMonth() * 100 + aDate.getDate();
        const bScore = bDate.getMonth() * 100 + bDate.getDate();
        const diff = aScore - bScore;
        return diff != 0 ? diff : a.name.localeCompare(b.name);
      }),
    });
  }

  function errorMessage(): JSX.Element {
    const state = form.state();
    if (state.type == "Failure") {
      return <ErrorMessage error={state.error} />;
    }
  }

  return (
    <Form onSubmit={form.submit} class="anniversaries-form">
      <Column>
        <H2>Anniversaries</H2>
        <div class="grid">
          <div class="label">Date</div>
          <div class="label">Name</div>
          <div></div>
          <Index each={form.fields().anniversaries}>
            {(anniversary, i) => (
              <>
                <TextBox
                  id={`date-${i}`}
                  invalid={!!form.errors().anniversaries?.[i]?.date}
                  errorId={undefined}
                  value={anniversary().date}
                  onChange={(value) => updateAnniversary(i, { date: value })}
                />
                <TextBox
                  id={`name-${i}`}
                  invalid={!!form.errors().anniversaries?.[i]?.name}
                  errorId={undefined}
                  value={anniversary().name}
                  onChange={(value) => updateAnniversary(i, { name: value })}
                />
                <IconButton
                  type="secondary"
                  icon={<Icon definition={faTrashAlt} />}
                  label="Delete anniversary"
                  onClick={() => removeAnniversary(i)}
                />
              </>
            )}
          </Index>
          <div class="help">Formatted YYYY-MM-DD</div>
          <div class="help">From 2 to 64 characters</div>
        </div>
        <Row>
          <Button
            type="secondary"
            label="Add anniversary"
            onClick={() => addAnniversary({ name: "", date: "" })}
          />
          <Button
            type="secondary"
            label="Sort anniversaries"
            onClick={() => sortAnniversaries()}
          />
        </Row>
        <RadioButtons
          id="language"
          label="Language"
          options={languageOptions}
          error={undefined}
          errorPlaceholder={false}
          value={form.fields().language}
          onChange={(value) => form.update({ language: value })}
        />
        <Button
          type="primary"
          label="Save"
          onClick="submit"
          busy={form.busy()}
        />
        {errorMessage()}
      </Column>
    </Form>
  );
}

const languageOptions: { value: Language; label: string }[] = [
  { value: "ENGLISH", label: "English" },
  { value: "NORWEGIAN", label: "Norwegian" },
];
