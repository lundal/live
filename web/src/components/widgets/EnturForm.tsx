import { createEffect, JSX, on } from "solid-js";
import {
  Button,
  Column,
  Field,
  Form,
  H2,
  RadioButtons,
  TextBox,
  createForm,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import {
  DirectionType,
  EnturConfig,
  Language,
  TransportMode,
  Widget,
} from "../../api/models";
import { updateWidget } from "../../api/boards";
import { regex, maxChars, minChars, required } from "../../validation/rules.ts";
import { validate } from "../../validation/validator.ts";
import { ErrorMessage } from "../ErrorMessage.tsx";

type Fields = {
  title: string;
  stopPlaceId: string;
  transportMode: TransportMode;
  directionType: DirectionType;
  language: Language;
};

type Errors = {
  title?: string;
  stopPlaceId?: string;
};

type Props = {
  boardId: string;
  widget: Widget<EnturConfig>;
  onUpdate: () => void;
};

const titleRules = [required(), minChars(2), maxChars(64)];
const stopPlaceIdRules = [
  required(),
  regex(/^NSR:StopPlace:\d+$/, "must match the format NSR:StopPlace:123"),
];

export function EnturForm(props: Props): JSX.Element {
  const form = createForm<Fields, Errors, Widget, RequestError>({
    initiator: () => ({
      title: props.widget.config.title,
      stopPlaceId: props.widget.config.stopPlaceId,
      transportMode: props.widget.config.transportMode,
      directionType: props.widget.config.directionType,
      language: props.widget.config.language,
    }),
    validator: (fields) => ({
      title: validate("Title", fields.title, titleRules),
      stopPlaceId: validate(
        "Stop place id",
        fields.stopPlaceId,
        stopPlaceIdRules,
      ),
    }),
    submitter: (fields) =>
      updateWidget(props.boardId, props.widget.id, {
        col: props.widget.col,
        row: props.widget.row,
        width: props.widget.width,
        height: props.widget.height,
        config: {
          type: "entur",
          title: fields.title,
          stopPlaceId: fields.stopPlaceId,
          transportMode: fields.transportMode,
          directionType: fields.directionType,
          language: fields.language,
        },
      }),
    onSuccess: () => props.onUpdate(),
  });

  createEffect(
    on(
      () => props.widget,
      () => {
        console.log("reset entur form");
        form.reset();
      },
    ),
  );

  function errorMessage(): JSX.Element {
    const state = form.state();
    if (state.type == "Failure") {
      return <ErrorMessage error={state.error} />;
    }
  }

  return (
    <Form onSubmit={form.submit}>
      <Column>
        <H2>Entur</H2>
        <Field
          id="title"
          label="Title"
          // help="Display name"
          error={form.errors().title}
          errorPlaceholder={false}
          children={(extra) => (
            <TextBox
              {...extra()}
              value={form.fields().title}
              onChange={(value) => form.update({ title: value })}
            />
          )}
        />
        <Field
          id="stop-place-id"
          label="Stop place id"
          // help="Example: NSR:StopPlace:123"
          error={form.errors().stopPlaceId}
          errorPlaceholder={false}
          children={(extra) => (
            <TextBox
              {...extra()}
              value={form.fields().stopPlaceId}
              onChange={(value) => form.update({ stopPlaceId: value })}
            />
          )}
        />
        <RadioButtons
          id="transport-mode"
          label="Transport mode"
          options={transportModeOptions}
          error={undefined}
          errorPlaceholder={false}
          value={form.fields().transportMode}
          onChange={(value) => form.update({ transportMode: value })}
        />
        <RadioButtons
          id="direction-type"
          label="Direction type"
          options={directionTypeOptions}
          error={undefined}
          errorPlaceholder={false}
          value={form.fields().directionType}
          onChange={(value) => form.update({ directionType: value })}
        />
        <RadioButtons
          id="language"
          label="Language"
          options={languageOptions}
          error={undefined}
          errorPlaceholder={false}
          value={form.fields().language}
          onChange={(value) => form.update({ language: value })}
        />
        <Button
          type="primary"
          label="Save"
          onClick="submit"
          busy={form.busy()}
        />
        {errorMessage()}
      </Column>
    </Form>
  );
}

const transportModeOptions: { value: TransportMode; label: string }[] = [
  { value: "BUS", label: "Bus" },
  { value: "METRO", label: "Metro" },
  { value: "RAIL", label: "Rail" },
  { value: "TRAM", label: "Tram" },
];

const directionTypeOptions: { value: DirectionType; label: string }[] = [
  { value: "INBOUND", label: "Inbound" },
  { value: "OUTBOUND", label: "Outbound" },
];

const languageOptions: { value: Language; label: string }[] = [
  { value: "ENGLISH", label: "English" },
  { value: "NORWEGIAN", label: "Norwegian" },
];
