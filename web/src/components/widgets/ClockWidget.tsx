import { JSX } from "solid-js";
import { createInterval, createSize } from "@lundal/zed-solid";
import { ClockConfig } from "../../api/models";
import { twoDigits } from "../../utils/TimeUtils";
import "./ClockWidget.css";

type Props = {
  config: ClockConfig;
};

export function ClockWidget(_: Props): JSX.Element {
  let element!: HTMLDivElement;

  const size = createSize(() => element);
  const date = createInterval(() => new Date());

  function getFontSize() {
    return Math.min(
      Math.floor(size().width / 3),
      Math.floor(size().height / 1.5),
    );
  }

  return (
    <div
      class="clock"
      ref={element}
      style={{ "font-size": `${getFontSize()}px` }}
    >
      <span class="time">
        {twoDigits(date().getHours())}:{twoDigits(date().getMinutes())}
      </span>
    </div>
  );
}
