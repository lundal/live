import {
  Accessor,
  createMemo,
  createResource,
  For,
  JSX,
  Match,
  Switch,
} from "solid-js";
import { createInterval, createSize } from "@lundal/zed-solid";
import { YrConfig, YrData } from "../../api/models";
import { getYrData } from "../../api/widgets";
import { toDate, twoDigits } from "../../utils/TimeUtils";
import "./YrWidget.css";
import { ErrorMessage } from "../ErrorMessage.tsx";

// Sizes
const margins = {
  top: 36,
  left: 36,
  right: 36,
  bottom: 1,
};

type Props = {
  config: YrConfig;
};

export function YrWidget(props: Props): JSX.Element {
  let element!: HTMLDivElement;

  const size = createSize(() => element);
  const now = createInterval(() => Date.now());

  const [data, dataActions] = createResource(props.config, (config) =>
    getYrData(config.latitude, config.longitude, config.altitude),
  );

  createInterval(() => dataActions.refetch(), 600_000);

  const width = () => size().width;
  const height = () => size().height;

  function emptyGraph(): JSX.Element {
    const cols = () => 24; // Should be even
    const rows = () => 8; // Should be even
    const dx = () => (width() - margins.left - margins.right) / cols();
    const dy = () => (height() - margins.top - margins.bottom) / rows();

    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox={`0 0 ${width()} ${height()}`}
      >
        <g class="grid">
          <For each={range(0, cols())}>
            {(x) => (
              <line
                x1={margins.left + dx() * x}
                x2={margins.left + dx() * x}
                y1={margins.top}
                y2={margins.left + dy() * rows()}
              />
            )}
          </For>
          <For each={range(0, rows())}>
            {(y) => (
              <line
                x1={margins.left}
                x2={margins.left + dx() * cols()}
                y1={margins.top + dy() * y}
                y2={margins.top + dy() * y}
              />
            )}
          </For>
        </g>
      </svg>
    );
  }

  function graph(data: Accessor<YrData>): JSX.Element {
    const forecasts = createMemo(() =>
      data()
        .forecasts.filter((data) => data.timestamp > now() - 3600000)
        .slice(0, 25),
    );
    const tempRange = createMemo(() =>
      calcTemperatureRange(forecasts().map((data) => data.temperature)),
    );

    // Calculate grid size
    const cols = () => 24; // Should be even
    const rows = () => tempRange().max - tempRange().min; // Should be even
    const dx = () => (width() - margins.left - margins.right) / cols();
    const dy = () => (height() - margins.top - margins.bottom) / rows();

    // Calculate symbol size
    const symbolSize = () => Math.min(dx() * 2 * 0.75, dy() * 2);
    const symbolOffset = () => symbolSize() * 0.75;

    // Draw grid with data
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox={`0 0 ${width()} ${height()}`}
      >
        <defs>
          <linearGradient
            id="temperature-gradient"
            x1="0"
            x2="0"
            y1={margins.top + dy() * (tempRange().max - 0.5)}
            y2={margins.top + dy() * (tempRange().max + 0.5)}
            gradientUnits="userSpaceOnUse"
          >
            <stop offset="0%" class="temperature-hot" />
            <stop offset="100%" class="temperature-cold" />
          </linearGradient>
        </defs>
        <g class="grid">
          <For each={range(0, cols())}>
            {(x) => (
              <line
                x1={margins.left + dx() * x}
                x2={margins.left + dx() * x}
                y1={margins.top}
                y2={margins.left + dy() * rows()}
              />
            )}
          </For>
          <For each={range(0, rows())}>
            {(y) => (
              <line
                x1={margins.left}
                x2={margins.left + dx() * cols()}
                y1={margins.top + dy() * y}
                y2={margins.top + dy() * y}
              />
            )}
          </For>
        </g>
        <g class="labels">
          <For each={range(0, cols()).filter((x) => x % 2 == 1)}>
            {(x) => (
              <text x={margins.left + dx() * x} y={margins.top * 0.5}>
                {twoDigits(toDate(forecasts()[x].timestamp).getHours())}
              </text>
            )}
          </For>
          <For each={range(0, rows()).filter((y) => y % 2 == 1)}>
            {(y) => (
              <text x={margins.left * 0.5} y={margins.top + dy() * y}>
                {`${tempRange().max - y}°`}
              </text>
            )}
          </For>
          <For each={range(0, rows()).filter((y) => y % 2 == 1)}>
            {(y) => (
              <text
                x={margins.left + margins.right * 0.5 + dx() * cols()}
                y={margins.top + dy() * y}
              >
                {rows() - y}
              </text>
            )}
          </For>
        </g>
        <g class="precipitation">
          <For each={range(0, cols() - 1)}>
            {(x) => {
              const mm = () => forecasts()[x].precipitation;
              return (
                <rect
                  x={margins.left + dx() * x}
                  y={margins.top + dy() * rows() - dy() * mm()}
                  width={dx()}
                  height={dy() * mm()}
                />
              );
            }}
          </For>
        </g>
        <g class="temperature">
          <path
            stroke="url('#temperature-gradient')"
            d={range(0, cols())
              .map((x) => {
                const temp = () => forecasts()[x].temperature;
                const xx = () => margins.left + dx() * x;
                const yy = () =>
                  margins.top + dy() * (tempRange().max - temp());
                return `${x == 0 ? "M" : "L"} ${xx()} ${yy()}`;
              })
              .join(" ")}
          />
        </g>
        <g class="symbols">
          <For each={range(0, cols()).filter((x) => x % 2 == 1)}>
            {(x) => {
              const temp = () => forecasts()[x].temperature;
              const symbol = () => forecasts()[x].symbol;
              const xx = () => margins.left + dx() * x;
              const yy = () => margins.top + dy() * (tempRange().max - temp());
              return (
                <image
                  x={xx() - symbolSize() / 2}
                  y={yy() - symbolSize() / 2 - symbolOffset()}
                  width={symbolSize()}
                  height={symbolSize()}
                  href={`/assets/weathericons/svg/${symbol()}.svg`}
                />
              );
            }}
          </For>
        </g>
      </svg>
    );
  }

  return (
    <div class="yr" ref={element}>
      <Switch fallback={emptyGraph()}>
        <Match when={data.error}>
          <ErrorMessage error={data.error} />
        </Match>
        <Match when={data.latest}>{(latest) => graph(latest)}</Match>
      </Switch>
    </div>
  );
}

function range(first: number, last: number): number[] {
  const list = [];
  for (let n = first; n <= last; n++) {
    list.push(n);
  }
  return list;
}

type TemperatureRange = {
  min: number;
  max: number;
};

function calcTemperatureRange(temps: number[]): TemperatureRange {
  const min = Math.min(...temps);
  const max = Math.max(...temps);
  const mid = Math.round((min + max) / 2 + 0.25);
  const delta = Math.max(Math.ceil((max - min) / 2) + 1, 4);
  return { min: mid - delta, max: mid + delta };
}
