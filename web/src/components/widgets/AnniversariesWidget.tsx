import { For, JSX, Show } from "solid-js";
import { createInterval, createSize } from "@lundal/zed-solid";
import { AnniversariesConfig, Anniversary, Language } from "../../api/models";
import "./AnniversariesWidget.css";

const DAY_IN_MS = 24 * 60 * 60 * 1000;

type Display = {
  day: string;
  name: string;
  years: number;
};

type Props = {
  config: AnniversariesConfig;
};

export function AnniversariesWidget(props: Props): JSX.Element {
  let element!: HTMLDivElement;

  const size = createSize(() => element);
  const date = createInterval(() => new Date());

  function getFontSize() {
    return Math.floor(size().height / 1.5);
  }

  const texts = () => getTexts(props.config.language);
  const displays = () =>
    mapToDisplay(props.config.anniversaries, texts(), date());

  return (
    <div
      class="anniversaries"
      ref={element}
      style={{ "font-size": `${getFontSize()}px` }}
    >
      <For each={displays()}>
        {(display) => (
          <span class="anniversary">
            <span class="day">{display.day}:</span>{" "}
            <span class="name">{display.name}</span> ({display.years})
          </span>
        )}
      </For>
      <Show when={displays().length == 0}>
        <span class="none">{texts().none}</span>
      </Show>
    </div>
  );
}

function mapToDisplay(
  anniversaries: Anniversary[],
  texts: Texts,
  now: Date,
): Display[] {
  let displays: Display[] = [];
  for (let i = 0; i < 7; i++) {
    const date = new Date(now.getTime() + i * DAY_IN_MS);
    anniversaries
      .filter((anniversary: Anniversary) =>
        sameDayOfYear(date, new Date(anniversary.date)),
      )
      .map((anniversary) => ({
        day: day(date, texts),
        name: anniversary.name,
        years: year(date) - year(new Date(anniversary.date)),
      }))
      .forEach((display: Display) => displays.push(display));
  }
  return displays;
}

function sameDayOfYear(a: Date, b: Date): boolean {
  return a.getMonth() == b.getMonth() && a.getDate() == b.getDate();
}

function day(date: Date, texts: Texts): string {
  const today = new Date();
  const tomorrow = new Date(today.getTime() + DAY_IN_MS);
  if (sameDayOfYear(date, today)) {
    return texts.today;
  }
  if (sameDayOfYear(date, tomorrow)) {
    return texts.tomorrow;
  }
  switch (date.getDay()) {
    case 1:
      return texts.monday;
    case 2:
      return texts.tuesday;
    case 3:
      return texts.wednesday;
    case 4:
      return texts.thursday;
    case 5:
      return texts.friday;
    case 6:
      return texts.saturday;
    default:
      return texts.sunday;
  }
}

function year(date: Date): number {
  return date.getFullYear();
}

type Texts = {
  none: string;
  today: string;
  tomorrow: string;
  monday: string;
  tuesday: string;
  wednesday: string;
  thursday: string;
  friday: string;
  saturday: string;
  sunday: string;
};

const ENGLISH: Texts = {
  none: "No upcoming anniversaries",
  today: "Today",
  tomorrow: "Tomorrow",
  monday: "Monday",
  tuesday: "Tuesday",
  wednesday: "Wednesday",
  thursday: "Thursday",
  friday: "Friday",
  saturday: "Saturday",
  sunday: "Sunday",
};

const NORWEGIAN: Texts = {
  none: "Ingen kommende merkedager",
  today: "I dag",
  tomorrow: "I morgen",
  monday: "Mandag",
  tuesday: "Tirsdag",
  wednesday: "Onsdag",
  thursday: "Torsdag",
  friday: "Fredag",
  saturday: "Lørdag",
  sunday: "Søndag",
};

function getTexts(language: Language) {
  switch (language) {
    case "ENGLISH":
      return ENGLISH;
    case "NORWEGIAN":
      return NORWEGIAN;
  }
}
