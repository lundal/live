import { createEffect, JSX, on } from "solid-js";
import {
  Button,
  Column,
  Field,
  Form,
  H2,
  TextBox,
  createForm,
} from "@lundal/zed-solid";
import { RequestError } from "@lundal/request";
import { Widget, YrConfig } from "../../api/models";
import { updateWidget } from "../../api/boards";
import { float, integer, required } from "../../validation/rules.ts";
import { validate } from "../../validation/validator.ts";
import { ErrorMessage } from "../ErrorMessage.tsx";

type Fields = {
  latitude: string;
  longitude: string;
  altitude: string;
};

type Errors = {
  latitude?: string;
  longitude?: string;
  altitude?: string;
};

type Props = {
  boardId: string;
  widget: Widget<YrConfig>;
  onUpdate: () => void;
};

const latitudeRules = [required(), float(-90, 90)];
const longitudeRules = [required(), float(-180, 180)];
const altitudeRules = [required(), integer(0, 10_000)];

export function YrForm(props: Props): JSX.Element {
  const form = createForm<Fields, Errors, Widget, RequestError>({
    initiator: () => ({
      latitude: props.widget.config.latitude.toString(),
      longitude: props.widget.config.longitude.toString(),
      altitude: props.widget.config.altitude.toString(),
    }),
    validator: (fields) => ({
      latitude: validate("Latitude", fields.latitude, latitudeRules),
      longitude: validate("Longitude", fields.longitude, longitudeRules),
      altitude: validate("Altitude", fields.altitude, altitudeRules),
    }),
    submitter: (fields) =>
      updateWidget(props.boardId, props.widget.id, {
        col: props.widget.col,
        row: props.widget.row,
        width: props.widget.width,
        height: props.widget.height,
        config: {
          type: "yr",
          latitude: parseFloat(fields.latitude),
          longitude: parseFloat(fields.longitude),
          altitude: parseInt(fields.altitude),
        },
      }),
    onSuccess: () => props.onUpdate(),
  });

  createEffect(
    on(
      () => props.widget,
      () => {
        console.log("reset yr form");
        form.reset();
      },
    ),
  );

  function errorMessage(): JSX.Element {
    const state = form.state();
    if (state.type == "Failure") {
      return <ErrorMessage error={state.error} />;
    }
  }

  return (
    <Form onSubmit={form.submit}>
      <Column>
        <H2>Yr</H2>
        <Field
          id="latitude"
          label="Latitude"
          // help="Latitude in degrees (north-south)"
          error={form.errors().latitude}
          errorPlaceholder={false}
          children={(extra) => (
            <TextBox
              {...extra()}
              style={{ width: "120px" }}
              value={form.fields().latitude}
              onChange={(value) => form.update({ latitude: value })}
            />
          )}
        />
        <Field
          id="longitude"
          label="Longitude"
          // help="Longitude in degrees (east-west)"
          error={form.errors().longitude}
          errorPlaceholder={false}
          children={(extra) => (
            <TextBox
              {...extra()}
              style={{ width: "120px" }}
              value={form.fields().longitude}
              onChange={(value) => form.update({ longitude: value })}
            />
          )}
        />
        <Field
          id="altitude"
          label="Altitude"
          // help="Altitude in meters above sea level"
          error={form.errors().altitude}
          errorPlaceholder={false}
          children={(extra) => (
            <TextBox
              {...extra()}
              style={{ width: "120px" }}
              value={form.fields().altitude}
              onChange={(value) => form.update({ altitude: value })}
            />
          )}
        />
        <Button
          type="primary"
          label="Save"
          onClick="submit"
          busy={form.busy()}
        />
        {errorMessage()}
      </Column>
    </Form>
  );
}
