export function toDate(timestamp: number): Date {
  return new Date(timestamp);
}

export function twoDigits(number: number): string {
  if (number < 10) {
    return "0" + number;
  } else {
    return "" + number;
  }
}
