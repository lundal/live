create table widgets
(
    id       text not null primary key,
    board_id text not null references boards (id) on delete cascade,
    col      int  not null,
    row      int  not null,
    width    int  not null,
    height   int  not null,
    config   text not null
);
