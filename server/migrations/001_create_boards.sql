create table boards
(
    id      text   not null primary key,
    user_id text   not null,
    name    text   not null,
    widgets text   not null,
    created bigint not null
);
