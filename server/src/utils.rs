use rand::{thread_rng, Rng};

// Crockford's Base32
const BASE_32: [char; 32] = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j',
    'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z',
];

// Equivalently unique and url safe as a UUID, but only 24 chars
pub fn random_id() -> String {
    let mut random_bytes = [0u8; 24];
    thread_rng().fill(&mut random_bytes);

    random_bytes
        .iter()
        .map(|byte| BASE_32[(byte % 32) as usize])
        .collect()
}
