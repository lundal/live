use env_logger::Target;
use log::{info, LevelFilter};
use std::error::Error;

use crate::clients::account::AccountClient;
use crate::clients::entur::EnturClient;
use crate::clients::yr::YrClient;
use crate::clients::Clients;

mod api;
mod clients;
mod config;
mod db;
mod jobs;
mod utils;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::builder()
        .target(Target::Stdout)
        .filter_level(LevelFilter::Warn)
        .filter_module(module_path!(), LevelFilter::Info)
        .try_init()?;

    info!("Parsing config");

    let config = config::parse();

    info!("Creating clients");

    let clients = Clients {
        account: AccountClient::new(&config.account_url, &config.api_key),
        entur: EnturClient::new(),
        yr: YrClient::new(),
    };

    info!("Connecting to database");

    let database = db::connect(&config.database_url).await?;

    info!("Starting background jobs");

    jobs::start(database.clone(), clients.clone()).await;

    info!("Starting web server");

    api::run(config, database, clients).await;

    Ok(())
}
