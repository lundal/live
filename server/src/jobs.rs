use std::collections::HashSet;

use chrono::Duration;
use log::error;
use sqlx::{Pool, Postgres};

use crate::clients::Clients;
use crate::db;

pub async fn start(database: Pool<Postgres>, clients: Clients) {
    tokio::spawn(async move {
        loop {
            tokio::time::sleep(Duration::minutes(10).to_std().unwrap()).await;
            if let Err(err) = delete_boards(database.clone(), clients.clone()).await {
                error!("Error in delete_boards: {}", err);
            }
        }
    });
}

pub async fn delete_boards(database: Pool<Postgres>, clients: Clients) -> Result<(), String> {
    let valid_accounts_ids: HashSet<String> = clients
        .account
        .list_accounts()
        .await
        .map_err(|err| format!("Reqwest error: {}", err))?
        .iter()
        .map(|account| account.id.clone())
        .collect();

    let account_ids = db::list_account_ids(&database)
        .await
        .map_err(|err| format!("Database error: {}", err))?;

    for account_id in account_ids {
        if !valid_accounts_ids.contains(&account_id) {
            db::delete_boards_for_account(&database, &account_id)
                .await
                .map_err(|err| format!("Database error: {}", err))?;
        }
    }

    Ok(())
}
