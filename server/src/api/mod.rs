use include_dir::{include_dir, Dir};
use serde::Serialize;
use sqlx::{Pool, Postgres};
use std::error::Error;
use std::path::Path;
use warp::http::StatusCode;
use warp::*;

use crate::clients::Clients;
use crate::config::Config;
use caches::Caches;
use errors::*;

mod auth;
mod boards;
mod caches;
mod config;
mod errors;
mod models;
mod widgets;

static STATIC: Dir<'_> = include_dir!("$CARGO_MANIFEST_DIR/static");

pub struct Context {
    session_cookie: Option<String>,
    config: Config,
    database: Pool<Postgres>,
    clients: Clients,
    caches: Caches,
}

pub async fn run(config: Config, database: Pool<Postgres>, clients: Clients) {
    let port = config.port;
    serve(routes(config, database, clients))
        .run(([0, 0, 0, 0], port))
        .await;
}

fn routes(
    config: Config,
    database: Pool<Postgres>,
    clients: Clients,
) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
    let caches = Caches::new();

    let with_context = || {
        // TODO: Figure out how to prevent double clone
        let config = config.clone();
        let database = database.clone();
        let clients = clients.clone();
        let caches = caches.clone();

        any()
            .and(cookie::optional("session"))
            .map(move |session_cookie: Option<String>| Context {
                session_cookie,
                config: config.clone(),
                database: database.clone(),
                clients: clients.clone(),
                caches: caches.clone(),
            })
    };

    let get_config = path!("v1" / "config")
        .and(get())
        .and(with_context())
        .then(config::get_config)
        .map(ok)
        .boxed();

    let create_board = path!("v1" / "boards")
        .and(post())
        .and(body::json())
        .and(with_context())
        .then(boards::create_board)
        .map(created)
        .boxed();

    let update_board = path!("v1" / "boards" / String)
        .and(put())
        .and(body::json())
        .and(with_context())
        .then(boards::update_board)
        .map(created)
        .boxed();

    let delete_board = path!("v1" / "boards" / String)
        .and(delete())
        .and(with_context())
        .then(boards::delete_board)
        .map(no_content)
        .boxed();

    let create_widget = path!("v1" / "boards" / String / "widgets")
        .and(post())
        .and(body::json())
        .and(with_context())
        .then(boards::create_widget)
        .map(created)
        .boxed();

    let update_widget = path!("v1" / "boards" / String / "widgets" / String)
        .and(put())
        .and(body::json())
        .and(with_context())
        .then(boards::update_widget)
        .map(ok)
        .boxed();

    let delete_widget = path!("v1" / "boards" / String / "widgets" / String)
        .and(delete())
        .and(with_context())
        .then(boards::delete_widget)
        .map(no_content)
        .boxed();

    let list_boards = path!("v1" / "boards")
        .and(get())
        .and(with_context())
        .then(boards::list_boards)
        .map(ok)
        .boxed();

    let get_board = path!("v1" / "boards" / String)
        .and(get())
        .and(with_context())
        .then(boards::get_board)
        .map(ok)
        .boxed();

    let get_transportation_data = path!("v1" / "widgets" / "entur")
        .and(get())
        .and(query())
        .and(with_context())
        .then(widgets::get_transportation_data)
        .map(ok)
        .boxed();

    let get_weather_data = path!("v1" / "widgets" / "yr")
        .and(get())
        .and(query())
        .and(with_context())
        .then(widgets::get_weather_data)
        .map(ok)
        .boxed();

    let api_routes = get_config
        .or(create_board)
        .or(update_board)
        .or(delete_board)
        .or(create_widget)
        .or(update_widget)
        .or(delete_widget)
        .or(list_boards)
        .or(get_board)
        .or(get_transportation_data)
        .or(get_weather_data)
        .recover(rejection_to_json);

    let statics = path::tail().and(get()).and_then(serve_static_files);

    path("api")
        .and(api_routes)
        .or(statics)
        .with(reply::with::header("cache-control", "no-store"))
        .with(reply::with::header(
            "content-security-policy",
            "default-src 'self'; img-src *; style-src 'self' 'unsafe-inline'", // TODO
        ))
}

fn ok<T: Serialize>(result: Result<T, ErrorResponse>) -> impl Reply {
    match result {
        Ok(res) => reply::with_status(reply::json(&res), StatusCode::OK),
        Err(err) => reply::with_status(reply::json(&err), StatusCode::from_u16(err.code).unwrap()),
    }
}

fn created<T: Serialize>(result: Result<T, ErrorResponse>) -> impl Reply {
    match result {
        Ok(res) => reply::with_status(reply::json(&res), StatusCode::CREATED),
        Err(err) => reply::with_status(reply::json(&err), StatusCode::from_u16(err.code).unwrap()),
    }
}

fn no_content(result: Result<(), ErrorResponse>) -> impl Reply {
    match result {
        Ok(res) => reply::with_status(reply::json(&res), StatusCode::NO_CONTENT),
        Err(err) => reply::with_status(reply::json(&err), StatusCode::from_u16(err.code).unwrap()),
    }
}

async fn serve_static_files(tail: path::Tail) -> Result<impl Reply, Rejection> {
    let path = if !tail.as_str().starts_with("assets/") {
        Path::new("index.html")
    } else {
        Path::new(tail.as_str())
    };
    if let Some(file) = STATIC.get_file(path) {
        let content_type = mime_guess::from_path(path)
            .first_or_octet_stream()
            .to_string();
        Ok(reply::with_header(
            file.contents(),
            "content-type",
            content_type,
        ))
    } else {
        Err(reject::not_found())
    }
}

async fn rejection_to_json(rejection: Rejection) -> Result<impl Reply, Rejection> {
    if rejection.is_not_found() {
        let status = StatusCode::NOT_FOUND;
        let error = ErrorResponse::new(status, None);
        Ok(reply::with_status(reply::json(&error), status))
    } else if let Some(e) = rejection.find::<body::BodyDeserializeError>() {
        let status = StatusCode::BAD_REQUEST;
        let error = ErrorResponse::new(status, e.source().map(|cause| cause.to_string()));
        Ok(reply::with_status(reply::json(&error), status))
    } else {
        Err(rejection)
    }
}
