use crate::api::auth::require_account;
use crate::api::errors::*;
use crate::api::models::{Account, AppLink, Config};
use crate::api::Context;

pub async fn get_config(context: Context) -> Result<Config, ErrorResponse> {
    return Ok(Config {
        account: require_account(&context).await.ok().map(|account| Account {
            id: account.id,
            username: account.username,
        }),
        account_url: context.config.account_url,
        app_links: context
            .config
            .app_links
            .split(',')
            .map(|app_link| {
                let parts: Vec<&str> = app_link.split('|').collect();
                AppLink {
                    name: parts[0].to_string(),
                    icon_url: parts[1].to_string(),
                    link_url: parts[2].to_string(),
                }
            })
            .collect(),
    });
}
