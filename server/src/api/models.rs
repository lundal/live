use chrono::NaiveDate;
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct Board {
    pub id: String,
    pub name: String,
    pub widgets: Vec<Widget>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct UpdateBoard {
    pub name: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Widget {
    pub id: String,
    pub col: i32,
    pub row: i32,
    pub width: i32,
    pub height: i32,
    pub config: WidgetConfig,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct UpdateWidget {
    pub col: i32,
    pub row: i32,
    pub width: i32,
    pub height: i32,
    pub config: WidgetConfig,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(
    rename_all = "camelCase",
    rename_all_fields = "camelCase",
    tag = "type"
)]
pub enum WidgetConfig {
    Anniversaries {
        anniversaries: Vec<Anniversary>,
        language: Language,
    },
    Clock {
        // Nothing yet
    },
    Entur {
        title: String,
        stop_place_id: String,
        transport_mode: TransportMode,
        direction_type: DirectionType,
        language: Language,
    },
    Yr {
        latitude: f32,
        longitude: f32,
        altitude: i32,
    },
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Anniversary {
    pub name: String,
    pub date: NaiveDate,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Language {
    ENGLISH,
    NORWEGIAN,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum TransportMode {
    AIR,
    BUS,
    COACH,
    METRO,
    RAIL,
    TRAM,
    WATER,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum DirectionType {
    INBOUND,
    OUTBOUND,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Config {
    pub account: Option<Account>,
    pub account_url: String,
    pub app_links: Vec<AppLink>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Account {
    pub id: String,
    pub username: String,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AppLink {
    pub name: String,
    pub icon_url: String,
    pub link_url: String,
}
