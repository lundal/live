use chrono::Utc;
use log::info;

use crate::api::auth::require_account;
use crate::api::errors::*;
use crate::api::models::*;
use crate::api::Context;
use crate::db;
use crate::utils::random_id;

pub async fn create_board(update: UpdateBoard, context: Context) -> Result<Board, ErrorResponse> {
    let account = require_account(&context).await?;

    let id = random_id();

    let board = db::Board {
        id: id,
        account_id: account.id,
        name: update.name,
        widgets: "".to_string(),
        created: Utc::now().timestamp_millis(),
    };

    db::insert_board(&context.database, &board)
        .await
        .map_err(ErrorResponse::database)?;

    info!(
        "Created board '{}' for account '{}'",
        board.id, board.account_id
    );

    Ok(to_api(&board, &Vec::new()))
}

pub async fn update_board(
    id: String,
    update: UpdateBoard,
    context: Context,
) -> Result<Board, ErrorResponse> {
    let mut board = db::get_board(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::board_not_found())?;

    let account = require_account(&context).await?;

    if board.account_id != account.id {
        return Err(ErrorResponse::permission_denied());
    }

    board.name = update.name;

    db::update_board(&context.database, &board)
        .await
        .map_err(ErrorResponse::database)?;

    info!(
        "Updated board '{}' for account '{}'",
        board.id, board.account_id
    );

    let widgets = db::list_widgets_for_board(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?;

    Ok(to_api(&board, &widgets))
}

pub async fn delete_board(id: String, context: Context) -> Result<(), ErrorResponse> {
    let board = db::get_board(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::board_not_found())?;

    let account = require_account(&context).await?;

    if board.account_id != account.id {
        return Err(ErrorResponse::permission_denied());
    }

    db::delete_board(&context.database, &board.id)
        .await
        .map_err(ErrorResponse::database)?;

    info!(
        "Deleted board '{}' for account '{}'",
        board.id, board.account_id
    );

    Ok(())
}

pub async fn create_widget(
    board_id: String,
    update: UpdateWidget,
    context: Context,
) -> Result<Widget, ErrorResponse> {
    let board = db::get_board(&context.database, &board_id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::board_not_found())?;

    let account = require_account(&context).await?;

    if board.account_id != account.id {
        return Err(ErrorResponse::permission_denied());
    }

    let id = random_id();

    let widget = db::Widget {
        id,
        board_id,
        col: update.col,
        row_: update.row,
        width: update.width,
        height: update.height,
        config: serde_json::to_string(&update.config).map_err(ErrorResponse::serde)?,
    };

    db::insert_widget(&context.database, &widget)
        .await
        .map_err(ErrorResponse::database)?;

    info!(
        "Created widget '{}' in board '{}'",
        widget.id, widget.board_id
    );

    Ok(widget_to_api(&widget))
}

pub async fn update_widget(
    board_id: String,
    widget_id: String,
    update: UpdateWidget,
    context: Context,
) -> Result<Widget, ErrorResponse> {
    let board = db::get_board(&context.database, &board_id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::board_not_found())?;

    let account = require_account(&context).await?;

    if board.account_id != account.id {
        return Err(ErrorResponse::permission_denied());
    }

    let mut widget = db::get_widget(&context.database, &widget_id, &board_id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::widget_not_found())?;

    widget.col = update.col;
    widget.row_ = update.row;
    widget.width = update.width;
    widget.height = update.height;
    widget.config = serde_json::to_string(&update.config).map_err(ErrorResponse::serde)?;

    db::update_widget(&context.database, &widget)
        .await
        .map_err(ErrorResponse::database)?;

    info!(
        "Updated widget '{}' in board '{}'",
        widget.id, widget.board_id
    );

    Ok(widget_to_api(&widget))
}

pub async fn delete_widget(
    board_id: String,
    widget_id: String,
    context: Context,
) -> Result<(), ErrorResponse> {
    let board = db::get_board(&context.database, &board_id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::board_not_found())?;

    let account = require_account(&context).await?;

    if board.account_id != account.id {
        return Err(ErrorResponse::permission_denied());
    }

    let widget = db::get_widget(&context.database, &widget_id, &board_id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::widget_not_found())?;

    db::delete_widget(&context.database, &widget.id, &widget.board_id)
        .await
        .map_err(ErrorResponse::database)?;

    info!(
        "Deleted widget '{}' in board '{}'",
        widget.id, widget.board_id
    );

    Ok(())
}

pub async fn list_boards(context: Context) -> Result<Vec<Board>, ErrorResponse> {
    let account = require_account(&context).await?;

    let boards = db::list_boards_for_account(&context.database, &account.id)
        .await
        .map_err(ErrorResponse::database)?;

    let mut ret = Vec::new();

    for board in boards {
        let widgets = db::list_widgets_for_board(&context.database, &board.id)
            .await
            .map_err(ErrorResponse::database)?;
        ret.push(to_api(&board, &widgets))
    }

    Ok(ret)
}

pub async fn get_board(id: String, context: Context) -> Result<Board, ErrorResponse> {
    let board = db::get_board(&context.database, &id)
        .await
        .map_err(ErrorResponse::database)?
        .ok_or(ErrorResponse::board_not_found())?;

    let widgets = db::list_widgets_for_board(&context.database, &board.id)
        .await
        .map_err(ErrorResponse::database)?;

    Ok(to_api(&board, &widgets))
}

fn to_api(board: &db::Board, widgets: &Vec<db::Widget>) -> Board {
    Board {
        id: board.id.clone(),
        name: board.name.clone(),
        widgets: widgets.iter().map(widget_to_api).collect(),
    }
}

fn widget_to_api(widget: &db::Widget) -> Widget {
    Widget {
        id: widget.id.clone(),
        col: widget.col,
        row: widget.row_,
        width: widget.width,
        height: widget.height,
        config: serde_json::from_str(&widget.config).expect("Invalid widget config in database"),
    }
}
