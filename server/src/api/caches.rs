use moka::future::Cache;
use std::time::Duration;

use crate::api::errors::ErrorResponse;
use crate::api::widgets::{TransportationData, WeatherData};
use crate::clients::account::Account;

#[derive(Clone)]
pub struct Caches {
    pub sessions: Cache<String, Result<Account, ErrorResponse>>,
    pub transportation_data: Cache<String, Result<TransportationData, ErrorResponse>>,
    pub weather_data: Cache<String, Result<WeatherData, ErrorResponse>>,
}

impl Caches {
    pub fn new() -> Self {
        Self {
            sessions: Cache::builder()
                .time_to_live(Duration::from_secs(10))
                .max_capacity(100_000)
                .build(),
            transportation_data: Cache::builder()
                .time_to_live(Duration::from_secs(5 * 60))
                .max_capacity(100_000)
                .build(),
            weather_data: Cache::builder()
                .time_to_live(Duration::from_secs(60 * 60))
                .max_capacity(100_000)
                .build(),
        }
    }
}
