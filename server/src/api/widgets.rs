use serde::{Deserialize, Serialize};

use crate::api::errors::*;
use crate::api::models::*;
use crate::api::Context;
use crate::clients::entur::*;

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TransportationDataQueryParams {
    pub stop_place_id: String,
    pub transport_mode: TransportMode,
    pub direction_type: DirectionType,
    pub language: Language,
}

impl TransportMode {
    fn value(&self) -> &str {
        match self {
            TransportMode::AIR => "air",
            TransportMode::BUS => "bus",
            TransportMode::COACH => "coach",
            TransportMode::METRO => "metro",
            TransportMode::RAIL => "rail",
            TransportMode::TRAM => "tram",
            TransportMode::WATER => "water",
        }
    }
}

impl DirectionType {
    fn values(&self) -> Vec<&str> {
        match self {
            DirectionType::INBOUND => vec!["inbound", "clockwise", "unknown"],
            DirectionType::OUTBOUND => vec!["outbound", "anticlockwise"],
        }
    }
}

impl Language {
    fn value(&self) -> &str {
        match self {
            Language::ENGLISH => "en",
            Language::NORWEGIAN => "no",
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct TransportationData {
    pub departures: Vec<Departure>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Departure {
    pub timestamp: i64,
    pub direction: String,
    pub destination: String,
    pub line: Line,
    pub situations: Vec<String>,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Line {
    pub code: String,
    pub name: String,
    pub color: String,
    pub text_color: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct WeatherDataQueryParams {
    pub latitude: f32,
    pub longitude: f32,
    pub altitude: i32,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct WeatherData {
    pub forecasts: Vec<WeatherForecast>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct WeatherForecast {
    pub timestamp: i64,
    pub symbol: String,
    pub temperature: f32,
    pub precipitation: f32,
    pub wind: f32,
}

pub async fn get_transportation_data(
    query_params: TransportationDataQueryParams,
    context: Context,
) -> Result<TransportationData, ErrorResponse> {
    let key = format!(
        "{}:{:?}:{:?}:{:?}",
        query_params.stop_place_id,
        query_params.transport_mode,
        query_params.direction_type,
        query_params.language,
    );
    context
        .caches
        .transportation_data
        .get_with(key, async {
            let variables = StopPlaceDepartureVariables {
                stop_place_id: query_params.stop_place_id,
                num_departures: 20,
                white_listed_modes: vec![query_params.transport_mode.value().to_string()],
            };
            let data = context
                .clients
                .entur
                .get_stop_place_departures(variables)
                .await
                .map_err(ErrorResponse::reqwest)?;

            let departures = data
                .stop_place
                .estimated_calls
                .iter()
                .filter(|it| {
                    query_params
                        .direction_type
                        .values()
                        .contains(&&*it.service_journey.direction_type)
                })
                .map(|it| Departure {
                    timestamp: it.expected_departure_time.timestamp_millis(),
                    direction: it.service_journey.direction_type.clone(),
                    destination: it.destination_display.front_text.clone(),
                    line: Line {
                        code: it.service_journey.line.public_code.clone(),
                        name: it.service_journey.line.name.clone(),
                        color: it
                            .service_journey
                            .line
                            .presentation
                            .colour
                            .clone()
                            .unwrap_or("999".to_string()),
                        text_color: it
                            .service_journey
                            .line
                            .presentation
                            .text_colour
                            .clone()
                            .unwrap_or("FFF".to_string()),
                    },
                    situations: it
                        .situations
                        .iter()
                        .filter_map(|situation| {
                            let summary: &str = situation
                                .summary
                                .iter()
                                .filter(|text| text.language == query_params.language.value())
                                .map(|text| text.value.trim())
                                .next()
                                .unwrap_or("");
                            let description: &str = situation
                                .description
                                .iter()
                                .filter(|text| text.language == query_params.language.value())
                                .map(|text| text.value.trim())
                                .next()
                                .unwrap_or("");
                            if !summary.is_empty() && !description.is_empty() {
                                let punctuation = [',', '.', ';', ':'];
                                return Some(format!(
                                    "{}. {}.",
                                    summary.trim_end_matches(&punctuation),
                                    description.trim_end_matches(&punctuation)
                                ));
                            }
                            if !summary.is_empty() {
                                return Some(summary.to_string());
                            }
                            if !description.is_empty() {
                                return Some(description.to_string());
                            }
                            return None;
                        })
                        .collect(),
                })
                .collect();

            Ok(TransportationData { departures })
        })
        .await
}

pub async fn get_weather_data(
    query_params: WeatherDataQueryParams,
    context: Context,
) -> Result<WeatherData, ErrorResponse> {
    // Reduce precision to help cache
    let lat = format!("{:.2}", query_params.latitude);
    let lon = format!("{:.2}", query_params.longitude);
    let alt = query_params.altitude - query_params.altitude % 20;

    let key = format!("{lat}:{lon}:{alt}");
    context
        .caches
        .weather_data
        .get_with(key, async {
            let data = context
                .clients
                .yr
                .get_compact_forecast(&lat, &lon, alt)
                .await
                .map_err(ErrorResponse::reqwest)?;

            let forecasts = data
                .properties
                .timeseries
                .iter()
                .take(32)
                .map(|it| WeatherForecast {
                    timestamp: it.time.timestamp_millis(),
                    symbol: it
                        .data
                        .next_1_hours
                        .as_ref()
                        .map(|it| it.summary.symbol_code.clone())
                        .unwrap_or("".to_string()),
                    temperature: it
                        .data
                        .instant
                        .as_ref()
                        .and_then(|it| it.details.air_temperature)
                        .unwrap_or(0f32),
                    precipitation: it
                        .data
                        .next_1_hours
                        .as_ref()
                        .and_then(|it| it.details.precipitation_amount)
                        .unwrap_or(0f32),
                    wind: it
                        .data
                        .instant
                        .as_ref()
                        .and_then(|it| it.details.wind_speed)
                        .unwrap_or(0f32),
                })
                .collect();

            Ok(WeatherData { forecasts })
        })
        .await
}
