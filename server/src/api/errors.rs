use log::error;
use serde::{Deserialize, Serialize};
use warp::http::StatusCode;

#[derive(Clone, Serialize, Deserialize)]
pub struct ErrorResponse {
    pub code: u16,
    pub reason: String,
    pub details: Option<String>,
}

impl ErrorResponse {
    pub fn new(status: StatusCode, details: Option<String>) -> Self {
        ErrorResponse {
            code: status.as_u16(),
            reason: status.to_string(),
            details,
        }
    }

    pub fn board_not_found() -> Self {
        ErrorResponse {
            code: 404,
            reason: "Board not found".into(),
            details: None,
        }
    }

    pub fn widget_not_found() -> Self {
        ErrorResponse {
            code: 404,
            reason: "Widget not found".into(),
            details: None,
        }
    }

    pub fn missing_session_cookie() -> Self {
        ErrorResponse {
            code: 401,
            reason: "Missing session cookie".into(),
            details: None,
        }
    }

    pub fn invalid_session_cookie() -> Self {
        ErrorResponse {
            code: 401,
            reason: "Invalid session cookie".into(),
            details: None,
        }
    }

    pub fn permission_denied() -> Self {
        ErrorResponse {
            code: 403,
            reason: "Permission denied".into(),
            details: None,
        }
    }

    pub fn database(error: sqlx::Error) -> Self {
        error!("Database error: {}", error);
        ErrorResponse {
            code: 500,
            reason: "Internal server error".into(),
            details: None,
        }
    }

    pub fn reqwest(error: reqwest::Error) -> Self {
        error!("Reqwest error: {}", error);
        ErrorResponse {
            code: 500,
            reason: "Internal server error".into(),
            details: None,
        }
    }

    pub fn serde(error: serde_json::Error) -> Self {
        error!("Serde error: {}", error);
        ErrorResponse {
            code: 500,
            reason: "Internal server error".into(),
            details: None,
        }
    }
}
