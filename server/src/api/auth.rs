use crate::api::errors::ErrorResponse;
use crate::api::Context;
use crate::clients::account::{Account, IdentifyAccount};

pub async fn require_account(context: &Context) -> Result<Account, ErrorResponse> {
    let session_cookie = context
        .session_cookie
        .as_ref()
        .ok_or(ErrorResponse::missing_session_cookie())?;

    context
        .caches
        .sessions
        .get_with(session_cookie.clone(), async {
            let account = context
                .clients
                .account
                .identify_account(&IdentifyAccount {
                    session_cookie: session_cookie.clone(),
                })
                .await
                .map_err(|err| match err.status() {
                    Some(..) => ErrorResponse::invalid_session_cookie(),
                    None => ErrorResponse::reqwest(err),
                })?;

            Ok(account)
        })
        .await
}
