use std::env::var;

#[derive(Clone)]
pub struct Config {
    pub database_url: String,
    pub account_url: String,
    pub port: u16,
    pub api_key: String,
    pub app_links: String,
}

pub fn parse() -> Config {
    Config {
        database_url: var("DATABASE_URL")
            .unwrap_or("postgres://test:test@localhost:3303/test".to_string()),
        account_url: var("ACCOUNT_URL").unwrap_or("http://localhost:3200".to_string()),
        port: var("PORT")
            .map(|value| value.parse().expect("invalid port"))
            .unwrap_or(3203),
        api_key: var("API_KEY")
            .unwrap_or("test".to_string()),
        app_links: var("APP_LINKS")
            .unwrap_or("Account|http://localhost:3200/icon.svg|http://localhost:3200/,Live|http://localhost:3203/icon.svg|http://localhost:3203/".to_string()),
    }
}
