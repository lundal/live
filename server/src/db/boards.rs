use sqlx::postgres::PgQueryResult;
use sqlx::{query, query_as, query_scalar, Error, FromRow, PgExecutor};

#[derive(Clone, Debug, FromRow)]
pub struct Board {
    pub id: String,
    pub account_id: String,
    pub name: String,
    pub widgets: String,
    pub created: i64,
}

pub async fn insert_board<'e, E: PgExecutor<'e>>(
    e: E,
    board: &Board,
) -> Result<PgQueryResult, Error> {
    query("insert into boards (id, account_id, name, widgets, created) values ($1, $2, $3, $4, $5)")
        .bind(&board.id)
        .bind(&board.account_id)
        .bind(&board.name)
        .bind(&board.widgets)
        .bind(&board.created)
        .execute(e)
        .await
}

pub async fn update_board<'e, E: PgExecutor<'e>>(
    e: E,
    board: &Board,
) -> Result<PgQueryResult, Error> {
    query("update boards set name = $1 where id = $2")
        .bind(&board.name)
        .bind(&board.id)
        .execute(e)
        .await
}

pub async fn delete_board<'e, E: PgExecutor<'e>>(
    e: E,
    id: &String,
) -> Result<PgQueryResult, Error> {
    query("delete from boards where id = $1")
        .bind(id)
        .execute(e)
        .await
}

pub async fn get_board<'e, E: PgExecutor<'e>>(e: E, id: &String) -> Result<Option<Board>, Error> {
    query_as("select * from boards where id = $1")
        .bind(id)
        .fetch_optional(e)
        .await
}

pub async fn list_boards_for_account<'e, E: PgExecutor<'e>>(
    e: E,
    account_id: &String,
) -> Result<Vec<Board>, Error> {
    query_as("select * from boards where account_id = $1")
        .bind(account_id)
        .fetch_all(e)
        .await
}

pub async fn delete_boards_for_account<'e, E: PgExecutor<'e>>(
    e: E,
    account_id: &String,
) -> Result<PgQueryResult, Error> {
    query("delete from boards where account_id = $1")
        .bind(account_id)
        .execute(e)
        .await
}

pub async fn list_account_ids<'e, E: PgExecutor<'e>>(e: E) -> Result<Vec<String>, Error> {
    query_scalar("select distinct account_id from boards")
        .fetch_all(e)
        .await
}
