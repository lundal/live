use sqlx::postgres::PgQueryResult;
use sqlx::{query, query_as, Error, FromRow, PgExecutor};

#[derive(Clone, Debug, FromRow)]
pub struct Widget {
    pub id: String,
    pub board_id: String,
    pub col: i32,
    #[sqlx(rename = "row")]
    pub row_: i32,
    pub width: i32,
    pub height: i32,
    pub config: String,
}

pub async fn insert_widget<'e, E: PgExecutor<'e>>(
    e: E,
    widget: &Widget,
) -> Result<PgQueryResult, Error> {
    query("insert into widgets (id, board_id, col, row, width, height, config) values ($1, $2, $3, $4, $5, $6, $7)")
        .bind(&widget.id)
        .bind(&widget.board_id)
        .bind(&widget.col)
        .bind(&widget.row_)
        .bind(&widget.width)
        .bind(&widget.height)
        .bind(&widget.config)
        .execute(e)
        .await
}

pub async fn update_widget<'e, E: PgExecutor<'e>>(
    e: E,
    widget: &Widget,
) -> Result<PgQueryResult, Error> {
    query("update widgets set col = $3, row = $4, width = $5, height = $6, config = $7 where id = $1 and board_id = $2")
        .bind(&widget.id)
        .bind(&widget.board_id)
        .bind(&widget.col)
        .bind(&widget.row_)
        .bind(&widget.width)
        .bind(&widget.height)
        .bind(&widget.config)
        .execute(e)
        .await
}

pub async fn delete_widget<'e, E: PgExecutor<'e>>(
    e: E,
    id: &String,
    board_id: &String,
) -> Result<PgQueryResult, Error> {
    query("delete from widgets where id = $1 and board_id = $2")
        .bind(id)
        .bind(board_id)
        .execute(e)
        .await
}

pub async fn get_widget<'e, E: PgExecutor<'e>>(
    e: E,
    id: &String,
    board_id: &String,
) -> Result<Option<Widget>, Error> {
    query_as("select * from widgets where id = $1 and board_id = $2")
        .bind(id)
        .bind(board_id)
        .fetch_optional(e)
        .await
}

pub async fn list_widgets_for_board<'e, E: PgExecutor<'e>>(
    e: E,
    board_id: &String,
) -> Result<Vec<Widget>, Error> {
    query_as("select * from widgets where board_id = $1")
        .bind(board_id)
        .fetch_all(e)
        .await
}
