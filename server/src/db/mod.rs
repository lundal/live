use sqlx::migrate::Migrator;
use sqlx::postgres::PgPoolOptions;
use sqlx::{migrate, Error, Pool, Postgres};

pub use boards::*;
pub use widgets::*;

mod boards;
mod widgets;

static MIGRATOR: Migrator = migrate!("./migrations");

pub async fn connect(url: &str) -> Result<Pool<Postgres>, Error> {
    let pool = PgPoolOptions::new().connect(url).await?;
    MIGRATOR.run(&pool).await?;
    Ok(pool)
}
