use std::time::Duration;

use reqwest::header::{HeaderMap, HeaderValue, AUTHORIZATION};
use reqwest::{Client, Error};
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct Account {
    pub id: String,
    pub username: String,
    pub r#type: String,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct IdentifyAccount {
    pub session_cookie: String,
}

#[derive(Clone)]
pub struct AccountClient {
    client: Client,
    base_url: String,
}

impl AccountClient {
    pub fn new(account_url: &String, api_key: &String) -> Self {
        let mut default_headers = HeaderMap::new();
        default_headers.insert(
            AUTHORIZATION,
            sensitive_header_value("api-key ".to_string() + api_key),
        );
        Self {
            client: Client::builder()
                .connect_timeout(Duration::from_secs(5))
                .read_timeout(Duration::from_secs(15))
                .default_headers(default_headers)
                .build()
                .expect("Failed to create account client"),
            base_url: account_url.clone() + "/api/v1",
        }
    }

    pub async fn list_accounts(&self) -> Result<Vec<Account>, Error> {
        let url = format!("{}{}", self.base_url, "/accounts");
        self.client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await
    }

    pub async fn identify_account(&self, body: &IdentifyAccount) -> Result<Account, Error> {
        let url = format!("{}{}", self.base_url, "/accounts/identify");
        self.client
            .post(url)
            .json(body)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await
    }
}

fn sensitive_header_value(value: String) -> HeaderValue {
    let mut header_value = HeaderValue::from_str(&value).expect("Failed to create header");
    header_value.set_sensitive(true);
    header_value
}
