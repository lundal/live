use chrono::{DateTime, Utc};
use std::time::Duration;

use reqwest::header::{HeaderMap, HeaderValue, USER_AGENT};
use reqwest::{Client, Error};
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct ForecastResponse {
    pub properties: ForecastProperties,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ForecastProperties {
    pub timeseries: Vec<ForecastTimeStep>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ForecastTimeStep {
    pub time: DateTime<Utc>,
    pub data: ForecastData,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ForecastData {
    pub instant: Option<ForecastDataInstant>,
    pub next_1_hours: Option<ForecastDataPeriod>,
    pub next_6_hours: Option<ForecastDataPeriod>,
    pub next_12_hours: Option<ForecastDataPeriod>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ForecastDataInstant {
    pub details: ForecastTimeInstant,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ForecastDataPeriod {
    pub details: ForecastTimePeriod,
    pub summary: ForecastSummary,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ForecastTimeInstant {
    pub air_pressure_at_sea_level: Option<f32>,
    pub air_temperature: Option<f32>,
    pub cloud_area_fraction: Option<f32>,
    pub cloud_area_fraction_high: Option<f32>,
    pub cloud_area_fraction_low: Option<f32>,
    pub cloud_area_fraction_medium: Option<f32>,
    pub dew_point_temperature: Option<f32>,
    pub fog_area_fraction: Option<f32>,
    pub relative_humidity: Option<f32>,
    pub wind_from_direction: Option<f32>,
    pub wind_speed: Option<f32>,
    pub wind_speed_of_gust: Option<f32>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ForecastTimePeriod {
    pub air_temperature_max: Option<f32>,
    pub air_temperature_min: Option<f32>,
    pub precipitation_amount: Option<f32>,
    pub precipitation_amount_max: Option<f32>,
    pub precipitation_amount_min: Option<f32>,
    pub probability_of_precipitation: Option<f32>,
    pub probability_of_thunder: Option<f32>,
    pub ultraviolet_index_clear_sky_max: Option<f32>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ForecastSummary {
    pub symbol_code: String,
}

#[derive(Clone)]
pub struct YrClient {
    client: Client,
    base_url: String,
}

impl YrClient {
    pub fn new() -> Self {
        let mut default_headers = HeaderMap::new();
        default_headers.insert(USER_AGENT, HeaderValue::from_static("live"));
        Self {
            client: Client::builder()
                .connect_timeout(Duration::from_secs(5))
                .read_timeout(Duration::from_secs(15))
                .default_headers(default_headers)
                .build()
                .expect("Failed to create yr client"),
            base_url: "https://api.met.no/weatherapi/locationforecast/2.0".to_string(),
        }
    }

    pub async fn get_compact_forecast(
        &self,
        lat: &String,
        lon: &String,
        altitude: i32,
    ) -> Result<ForecastResponse, Error> {
        let url = format!(
            "{}/compact?lat={lat}&lon={lon}&altitude={altitude}",
            self.base_url
        );
        self.client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await
    }
}
