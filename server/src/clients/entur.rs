use chrono::{DateTime, Utc};
use std::time::Duration;

use indoc::indoc;
use reqwest::header::{HeaderMap, HeaderValue};
use reqwest::{Client, Error};
use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct GraphQlRequest<T> {
    pub variables: T,
    pub query: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct GraphQlResponse<T> {
    pub data: T,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StopPlaceDepartureVariables {
    pub stop_place_id: String,
    pub num_departures: i32,
    pub white_listed_modes: Vec<String>,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StopPlaceDepartureData {
    pub stop_place: StopPlace,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StopPlace {
    pub id: String,
    pub name: String,
    pub estimated_calls: Vec<EstimatedCall>,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct EstimatedCall {
    pub realtime: bool,
    pub aimed_departure_time: DateTime<Utc>,
    pub expected_departure_time: DateTime<Utc>,
    pub destination_display: DestinationDisplay,
    pub service_journey: ServiceJourney,
    pub situations: Vec<Situation>,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DestinationDisplay {
    pub front_text: String,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ServiceJourney {
    pub direction_type: String,
    pub line: Line,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Line {
    pub id: String,
    pub name: String,
    pub public_code: String,
    pub presentation: Presentation,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Presentation {
    pub colour: Option<String>,
    pub text_colour: Option<String>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Situation {
    pub summary: Vec<Text>,
    pub description: Vec<Text>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Text {
    pub language: String,
    pub value: String,
}

#[derive(Clone)]
pub struct EnturClient {
    client: Client,
    base_url: String,
}

impl EnturClient {
    pub fn new() -> Self {
        let mut default_headers = HeaderMap::new();
        default_headers.insert("ET-Client-Name", HeaderValue::from_static("lundal-live"));
        Self {
            client: Client::builder()
                .connect_timeout(Duration::from_secs(5))
                .read_timeout(Duration::from_secs(15))
                .default_headers(default_headers)
                .build()
                .expect("Failed to create entur client"),
            base_url: "https://api.entur.io/journey-planner/v3/graphql".to_string(),
        }
    }

    pub async fn get_stop_place_departures(
        &self,
        variables: StopPlaceDepartureVariables,
    ) -> Result<StopPlaceDepartureData, Error> {
        let request = GraphQlRequest {
            variables,
            query: indoc! {"
                query ($stopPlaceId: String!, $numDepartures: Int!, $whiteListedModes: [TransportMode]!) {
                  stopPlace(id: $stopPlaceId) {
                    id
                    name
                    estimatedCalls(numberOfDepartures: $numDepartures, whiteListedModes: $whiteListedModes) {
                      realtime
                      aimedDepartureTime
                      expectedDepartureTime
                      destinationDisplay {
                        frontText
                      }
                      serviceJourney {
                        directionType
                        line {
                          id
                          name
                          publicCode
                          presentation {
                            colour
                            textColour
                          }
                        }
                      }
                      situations {
                        summary {
                          language
                          value
                        }
                        description {
                          language
                          value
                        }
                      }
                    }
                  }
                }
                "}.to_string()
        };
        self.graphql(&request).await.map(|response| response.data)
    }

    async fn graphql<VARIABLES: Serialize, DATA: for<'a> Deserialize<'a>>(
        &self,
        request: &GraphQlRequest<VARIABLES>,
    ) -> Result<GraphQlResponse<DATA>, Error> {
        self.client
            .post(&self.base_url)
            .json(request)
            .send()
            .await?
            .error_for_status()?
            .json()
            .await
    }
}
