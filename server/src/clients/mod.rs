use account::AccountClient;
use entur::EnturClient;
use yr::YrClient;

pub mod account;
pub mod entur;
pub mod yr;

#[derive(Clone)]
pub struct Clients {
    pub account: AccountClient,
    pub entur: EnturClient,
    pub yr: YrClient,
}
