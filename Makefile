.PHONY: push image binary test

image_name = registry.gitlab.com/lundal/live
image_version = $(shell git branch --show-current)

server_binary = server/target/application
web_index = web/dist/index.html

# Push docker image to registry
push: image
	docker push $(image_name):$(image_version)

# Build docker image
image: $(server_binary) Dockerfile
	docker build --tag $(image_name):$(image_version) .

# Build binary
binary: $(server_binary)

# Build server binary
$(server_binary): $(shell find server/src -type f) $(shell find server -maxdepth 1 -type f) $(web_index)
	cd server && cargo build --release --locked
	mv server/target/release/live $(server_binary)

# Build web resources
$(web_index): $(shell find web/src -type f) $(shell find web -maxdepth 1 -type f)
	cd web && npm ci
	cd web && npm run clean
	cd web && npm run build

# Run integration tests
test:
	cd test && yarn install
	cd test && yarn test
