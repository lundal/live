import { expect, test } from "@playwright/test";
import { gotoLive, signIn } from "./utils";

test.beforeEach(async ({ page }) => {
  await signIn(page);
});

test.describe("Home", () => {
  test.beforeEach(async ({ page }) => {
    await gotoLive(page, "/");
  });

  test("should display boards", async ({ page }) => {
    await expect(page.locator("main")).toContainText("Boards");
  });
});
