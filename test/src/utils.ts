import { Page } from "@playwright/test";

export async function gotoLive(page: Page, path: string): Promise<void> {
  const liveUrl = process.env.LIVE_URL || "http://localhost:3203";
  await page.goto(liveUrl + path);
}

export async function gotoAccount(page: Page, path: string): Promise<void> {
  const accountUrl = process.env.ACCOUNT_URL || "http://localhost:3200";
  await page.goto(accountUrl + path);
}

export async function signIn(
  page: Page,
  username: string = "normaluser",
  password: string = "password1234",
): Promise<void> {
  await gotoAccount(page, "/sign-in");
  await page.locator("id=username").fill(username);
  await page.locator("id=password").fill(password);
  await page.locator("button >> text='Sign in'").click();
  await page.waitForNavigation();
}
