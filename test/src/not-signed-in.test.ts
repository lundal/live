import { expect, test } from "@playwright/test";
import { gotoLive } from "./utils";

test.describe("Home", () => {
  test.beforeEach(async ({ page }) => {
    await gotoLive(page, "/");
  });

  test("should redirect to sign in", async ({ page }) => {
    await expect(page.locator("main")).toContainText("Sign in");
  });
});
